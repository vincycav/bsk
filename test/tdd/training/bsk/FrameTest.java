package tdd.training.bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class FrameTest {

	/*@Test
	public void test() throws Exception{
		fail("Not yet implemented");
	}*/
	
	@Test
	//Tests user story 1
	public void testGetThrow() throws BowlingException {
		int firstThrow = 1;
		int secondThrow = 2;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(firstThrow, frame.getFirstThrow());
		assertEquals(secondThrow, frame.getSecondThrow());
	}
	
	@Test
	//Tests user story 2
	public void testGetFrameScore() throws BowlingException {
		int firstThrow = 1;
		int secondThrow = 2;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(firstThrow+secondThrow, frame.getScore());
	}
	
	@Test
	//Tests user story 3
	public void testGetFrameAtIndex() throws BowlingException {
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame(1, 5));
		frames.add(new Frame(2, 5));
		frames.add(new Frame(1, 1));
		frames.add(new Frame(4, 2));
		frames.add(new Frame(8, 0));
		frames.add(new Frame(2, 3));
		frames.add(new Frame(1, 3));
		frames.add(new Frame(1, 6));
		frames.add(new Frame(2, 0));
		frames.add(new Frame(10, 0));

		for(int i = 0; i < 10; i++)
			game.addFrame(frames.get(i));
		
		for(int j = 0; j < 10; j++)
			assertEquals(true, frames.get(j).equals(game.getFrameAt(j)));
	}
	
	@Test
	//Tests user story 4
	public void testGetGameScore() throws BowlingException {
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame(1, 5));
		frames.add(new Frame(2, 5));
		frames.add(new Frame(1, 1));
		frames.add(new Frame(4, 2));
		frames.add(new Frame(8, 0));
		frames.add(new Frame(2, 3));
		frames.add(new Frame(1, 3));
		frames.add(new Frame(1, 6));
		frames.add(new Frame(2, 0));
		frames.add(new Frame(10, 0));

		for(int i = 0; i < 10; i++)
			game.addFrame(frames.get(i));
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		assertEquals(score, game.calculateScore());
	}
	
	@Test
	//Tests user story 5
	public void testGetGameScoreWithSpare() throws BowlingException {
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame(1, 9));
		frames.add(new Frame(3, 6));
		frames.add(new Frame(7, 2));
		frames.add(new Frame(3, 6));
		frames.add(new Frame(4, 4));
		frames.add(new Frame(5, 3));
		frames.add(new Frame(0, 10));
		frames.add(new Frame(0, 5));
		frames.add(new Frame(8, 1));
		frames.add(new Frame(1, 9));

		for(int i = 0; i < 10; i++)
			game.addFrame(frames.get(i));
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		
		assertEquals(score+3, game.calculateScore());
	}
	
	@Test
	//Tests user story 6
	public void testGetGameScoreWithStrike() throws BowlingException {
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame(10, 0));
		frames.add(new Frame(3, 6));
		frames.add(new Frame(7, 2));
		frames.add(new Frame(3, 6));
		frames.add(new Frame(4, 4));
		frames.add(new Frame(5, 3));
		frames.add(new Frame(3, 3));
		frames.add(new Frame(4, 5));
		frames.add(new Frame(8, 1));
		frames.add(new Frame(2, 6));

		for(int i = 0; i < 10; i++)
			game.addFrame(frames.get(i));
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		
		assertEquals(score+9, game.calculateScore());
	}
	
	@Test
	//Tests user story 7
	public void testGetGameScoreWithStrikeAndSpare() throws BowlingException {
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame(10, 0));
		frames.add(new Frame(4, 6));
		frames.add(new Frame(7, 2));
		frames.add(new Frame(3, 6));
		frames.add(new Frame(4, 4));
		frames.add(new Frame(5, 3));
		frames.add(new Frame(3, 3));
		frames.add(new Frame(4, 5));
		frames.add(new Frame(8, 1));
		frames.add(new Frame(2, 6));

		for(int i = 0; i < 10; i++)
			game.addFrame(frames.get(i));
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		
		assertEquals(score+10+7, game.calculateScore());
	}
	
	@Test
	//Tests user story 8
	public void testGetGameScoreWithMultipleStrike() throws BowlingException {
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame(8, 2));
		frames.add(new Frame(5, 5));
		frames.add(new Frame(7, 2));
		frames.add(new Frame(3, 6));
		frames.add(new Frame(4, 4));
		frames.add(new Frame(5, 3));
		frames.add(new Frame(3, 3));
		frames.add(new Frame(4, 5));
		frames.add(new Frame(8, 2));
		frames.add(new Frame(2, 8));

		for(int i = 0; i < 10; i++)
			game.addFrame(frames.get(i));
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		assertEquals(score+5+7+2, game.calculateScore());
	}
	
	@Test
	//Tests user story 9
	public void testGetGameScoreWithMultipleSpares() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (8,2));
		frames.add(new Frame (5,5));
		frames.add(new Frame (7,2));
		frames.add(new Frame (3,6));
		frames.add(new Frame (4,4));
		frames.add(new Frame (5,3));
		frames.add(new Frame (3,3));
		frames.add(new Frame (4,5));
		frames.add(new Frame (8,1));
		frames.add(new Frame (2,6));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		assertEquals(score+5+7, game.calculateScore());
	}
	
	
	@Test
	//Tests user story 10
	public void testGetGameScoreWithSpareLastFrame() throws BowlingException {
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame(1, 5));
		frames.add(new Frame(3, 6));
		frames.add(new Frame(7, 2));
		frames.add(new Frame(3, 6));
		frames.add(new Frame(4, 4));
		frames.add(new Frame(5, 3));
		frames.add(new Frame(3, 3));
		frames.add(new Frame(4, 5));
		frames.add(new Frame(8, 1));
		frames.add(new Frame(2, 8));

		for(int i = 0; i < 10; i++)
			game.addFrame(frames.get(i));
		
		game.setFirstBonusThrow(7);
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		
		assertEquals(score+game.getFirstBonusThrow(), game.calculateScore());
	}
	
	@Test
	//Tests user story 11
	public void testGetGameScoreWithStrikeLastFrame() throws BowlingException {
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame(1, 5));
		frames.add(new Frame(3, 6));
		frames.add(new Frame(7, 2));
		frames.add(new Frame(3, 6));
		frames.add(new Frame(4, 4));
		frames.add(new Frame(5, 3));
		frames.add(new Frame(3, 3));
		frames.add(new Frame(4, 5));
		frames.add(new Frame(8, 1));
		frames.add(new Frame(10, 0));

		for(int i = 0; i < 10; i++)
			game.addFrame(frames.get(i));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		
		assertEquals(score+game.getFirstBonusThrow()+game.getSecondBonusThrow(), game.calculateScore());
	}
	
	@Test
	//Tests user story 12
	public void testGetPerfectGameScore() throws BowlingException {
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));

		for(int i = 0; i < 10; i++)
			game.addFrame(frames.get(i));
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		
		assertEquals(300, game.calculateScore());
	}

}
